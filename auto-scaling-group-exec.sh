#!/bin/bash
# 
# title         : auto-scaling-group-exec.sh
# description   : This script for executing a command on all hosts of AWS EC2 Auto Scaling Group via SSH.
# author        : Oleksii Pavliuk pavliuk.aleksey@gmail.com
# date          : 10/22/2020
# version       : 1.0.0
# usage         : Usage: ./auto-scaling-group-exec.sh [OPTIONS]
# notes         : Before using this script make sure that the environment has read-access
#                 permissions to AWS EC2 && AWS EC2 Auto Scaling, and AWS CLI v2
#                 is installed and configured.
#
#                 [!] SSH does via Identity File (Private Key) without password.
#

#
# -e: option instructs bash to immediately exit if
# any command has a non-zero exit status.
#
set -e

#
# Global variables.
#
AUTO_SCALING_GROUP_NAME=""
SSH_USER=""
SSH_IDENTITY_FILE="/"
EXEC_CMD=""

# Common:
AWS_REGION="ap-southeast-2"
LOG_FILE_PATH="./auto-scaling-group-exec.log"

cmdname=$(basename $0)

#
# The global variable about instances ID's and IP's of auto scaling group.
#
instances_ids=()
instances_ips=()

#
# ERROR Logging.
# Logging to syslog and console output.
#
# Arguments: $@ - error messages
# Return   : None
#
echoerr()
{
    date=`date`
    # echo "$date $cmdname $@" >> $LOG_FILE_PATH
    echo "$@" 1>&2;
}

#
# INFO Logging.
# Logging to syslog and console output.
#
# Arguments: $@ - log messages
# Return   : None
#
log()
{
    date=`date`
    # echo "$date $cmdname $@" >> $LOG_FILE_PATH
    echo "$@"
}

#
# Usage.
# Show variables which you must or can set like arguments.
#
# Arguments: None
# Return   : None
#
usage()
{
    cat << USAGE >&2
##############################################################################

Name       : $cmdname
Description: This script for executing a command on all hosts of AWS EC2 Auto Scaling
             Group via SSH.
Notes      : Before using this script make sure that the environment has read-access
             permissions to AWS EC2 && AWS EC2 Auto Scaling, and AWS CLI v2 is installed
             and configured

             [!] SSH does via Identity File (Private Key) without password.

Usage: ./$cmdname [OPTIONS]
  OPTIONS:
     [required]:
        --auto-scaling-group-name ARG : a name of AWS Auto Scale Group
        --ssh-user                    : a SSH Username
        --ssh-identity-file           : a SSH Identity file (Private Key)
        --exec-cmd                    : a command which need to execute on hosts of
                                        auto scaling group via SSH
     [optional]:
        --aws-region ARG              : AWS Region [default: $AWS_REGION]
        --log-file-path ARG           : path to log file for logging [default: $LOG_FILE_PATH]
        -h | --help                   : show this usage

Example: ./$cmdname --auto-scaling-group-name {GROUP_NAME} \
--ssh-user {USERNAME} \
--ssh-identity-file {PATH_TO_FILE} \
--exec-cmd {CMD}
USAGE
    exit 1
}

#
# Checking connection to AWS Auto Scaling.
#
# Arguments: None
# Return   : None
#
check_aws_autoscaling_connection()
{
    log "==> Connecting to AWS Auto Scaling..."
    aws autoscaling describe-auto-scaling-groups --region $AWS_REGION 1>/dev/null
    log "<== [!] Connected."
}

#
# Checking connection to AWS EC2.
#
# Arguments: None
# Return   : None
#
check_aws_ec2_connection()
{
    log "==> Connecting to AWS EC2..."
    aws ec2 describe-instances --region $AWS_REGION 1>/dev/null
    log "<== [!] Connected."
}

#
# Checking connection to AWS Services.
#
# Arguments: None
# Return   : None
#
check_aws_connection()
{
    log "=> Checking connection to AWS Services..."
    check_aws_autoscaling_connection
    check_aws_ec2_connection
    log "<= [!] Connections passed successfully."
}

#
# Request to get instances ID's of Auto Scaling Group
# to AWS Auto Scaling.
#
# Arguments: $@ - a auto scaling group name
# Return   : None
#
get_instances_ids_auto_scaling_group()
{
    unset auto_scaling_group_instances_ids
    group_name="$@"

    log ">>> Requesting to get instances ID's of Auto Scaling Group [$group_name] to AWS Auto Scaling..."
    instances_ids=($(
        aws autoscaling describe-auto-scaling-groups \
            --auto-scaling-group-names "$group_name" \
            --query 'AutoScalingGroups[].Instances' \
            --output text \
            --region $AWS_REGION | awk '{print $3}'
        ))
    log "<<< [!] SUCCESS >>>"
}

#
# Request to get instances IP's by ID's to AWS EC2.
#
# Arguments: $@ - an array of ID's
# Return   : None
#
get_instances_ips_by_ids()
{
    unset instances_ips
    ids="$@"

    log ">>> Requesting to get instances IP's by ID's to AWS EC2..."
    for id in "${ids[@]}"
    do
        instances_ips+=($(
            aws ec2 describe-instances \
                --instance-ids "$id" \
                --query 'Reservations[].Instances[].PrivateIpAddress' \
                --output text \
                --region $AWS_REGION
        ))
    done
    log "<<< [!] SUCCESS >>>"
}

#
# Execute a Command via SSH.
# Execute a command on the host via SSH.
#
# Arguments: $@ - a command
# Return   : None
#
exec_ssh_cmd()
{
    host_ips="${instances_ips[@]}"
    cmd="$@"

    log "> Execute a command [$cmd] in all hosts of Auto Scaling Group [$AUTO_SCALING_GROUP_NAME] via SSH..."
    for ip in "${host_ips[@]}"
    do
        log ">> SSH to host $ip..."
#        ssh -i $SSH_IDENTITY_FILE $SSH_USER@$ip "$cmd"

#       >> Test...
        log ">> Changed IP to Test EC2: $ip => 172.31.2.178..."
        ssh -i $SSH_IDENTITY_FILE $SSH_USER@172.31.2.178 "$cmd"
#       ...Test <<

        log "<< Success."
    done
    log "< Completed. >"
}

#
# Main Function.
#
# Arguments: None
# Return   : None
#
main()
{
    log "The process of executing a command on hosts of Auto Scaling Group is begun..."
    check_aws_connection
    get_instances_ids_auto_scaling_group $AUTO_SCALING_GROUP_NAME
    get_instances_ips_by_ids "${instances_ids[@]}"
    exec_ssh_cmd "$EXEC_CMD"
    log "[!] The process is completed."
}


# --------------------| ARGUMENTS |--------------------

#
# Checking empty argument.
# If it isn't empty, the function will return an argument.
# Otherwise, it will show error message and usage.
#
# Arguments: $1 - argument
# Return   : argument
#
check_argument()
{
    if [ ! -z $1 ];
    then
        echo "$@"
    else
        echoerr "[ERROR] There is an empty argument!"
        usage
    fi
}

#
# Arguments Parsing.
# Parses arguments and give error with usage
# if argument didn't find.
#
while [ $# -gt 0 ]; do
  case "$1" in
    --auto-scaling-group-name)
      shift 1
      AUTO_SCALING_GROUP_NAME=$(check_argument $1)
    ;;
    --ssh-user)
      shift 1
      SSH_USER=$(check_argument $1)
    ;;
    --ssh-identity-file)
      shift 1
      SSH_IDENTITY_FILE=$(check_argument $1)
    ;;
    --exec-cmd)
      shift 1
      EXEC_CMD=$(check_argument $1)
    ;;
    --aws-region)
      shift 1
      AWS_REGION=$(check_argument $1)
    ;;
    --log-file-path)
      shift 1
      LOG_FILE_PATH=$(check_argument $1)
    ;;
    -h | --help)
      usage
    ;;
    *)
      echoerr "Unknown argument: $1"
      usage
    ;;
    esac
  shift 1
done

#
# Required Arguments Checking.
#
if [ -z "$AUTO_SCALING_GROUP_NAME" ] || [ -z "$SSH_USER" ] || [ -z "$SSH_IDENTITY_FILE" ] || [ -z "$EXEC_CMD" ];
then
    echoerr "[ERROR] Required arguments are --auto-scaling-group-name, --ssh-user, --ssh-identity-file, and --exec-cmd."
    usage
fi

main
